oath user: kurotetsuka
oath req remaining: 4937
repo name: rails/rails
repo exists?: true
repo has issues?: true
repo issue length: 30
issue number: 17777, state: open, comment count: 5
    user: ngauthier, comment: @sgrif ping :heart: 
    user: ngauthier, comment: Runtimes by version before patch:

- 1.9.3: 38m15s
- 2.0.0: 21m08s
- 2.1: 20m24s
- head: 21m40s

After patch:

- 1.9.3: 11m41s
- 2.0.0: 7m46s
- 2.1: 6m41s
- head: 7m01s

    user: sgrif, comment: NICE!!!! :heart:

On Tue, Nov 25, 2014, 7:27 PM Nick Gauthier <notifications@github.com>
wrote:

> Runtimes by version before patch:
>
>    - 1.9.3: 38m15s
>    - 2.0.0: 21m08s
>    - 2.1: 20m24s
>    - head: 21m40s
>
> After patch:
>
>    - 1.9.3: 11m41s
>    - 2.0.0: 7m46s
>    - 2.1: 6m41s
>    - head: 7m01s
>
> —
> Reply to this email directly or view it on GitHub
> <https://github.com/rails/rails/pull/17777#issuecomment-64506132>.
>
    user: ngauthier, comment: I borrowed the idea from a project called "spring". Have you heard of it? :laughing: 
    user: matthewd, comment: Err, "warm test dependencies" doesn't sound terribly isolated.
