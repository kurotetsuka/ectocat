# metrics we'll try to use

## metric analysis method
 - determine average and standard deviation for metric for a given time block ( eg: one week )
 - model the trend of the average and standard devation for larger time blocks ( eg: 3 months )
 - assess trend stability and valence ( wording? )
 - use the model to predict the trend for future larger time blocks

## non-nlp ideas
 - open issue staleness
	 - how long issues that were open at timepoint had been open
	 - open ( forgotten ) issue backlog
 - developer count and rating
	 - developers identified and classified by commit presence
 - issue resolution time
	 - how quickly closed issues get resolved
 - developer response time
	 - how quickly developers respond ( comment ) to issues and non-developer comments
 - issue origin
	 - made by a developer?
 - references to issues in commit message
 - references to commits in issue comments
 - fork activity ( commits per time block accross forks )
 - commit rate
 - 
 - 
 - 
 - 
 - 

## nlp ideas
 - emotional trends in discussions for each primary emotion
	 - eg: lots of anger to lots of love
 - post-close comment emotion
 - love in a comment referencing a commit
 - 

## chosen ( non-nlp )

## chosen ( nlp )


## implemented

