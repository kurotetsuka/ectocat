## Primary research question
Can we assess the health of open source projects by the way people talk about them?

## Secondary question
Which is more useful for autonomously determining the health of projects: metrics or emotion mining?
