## Project Ectocat :: Open Source Health Analytics ##

Author: [kurotetsuka](github.com/kurotetsuka)  
License: [gnu-gpl-v3.0](license.md) ([details](legal/gnu-gpl-v3.0.md))  

## Development Setup
There are a few system dependencies: ruby and graphicsmagick.  
On Archlinux, the following command will install them:  
```bash
sudo pacman -S ruby graphicsmagick
```

There are a few gem dependencies:  
```bash
gem install dotenv erubis haml json octokit sinatra sinatra-contrib thin
```

The database must be created and populated, and some files have to be generated.
```
rake db:create mine crunch gen
```

To run the web app, run `rake launch`. By default it runs on port 8080, so navigate to [`localhost:8080/`](http://localhost:8080/) to have a look at it.

To run the rest api, run `rake serve`. The rest api must be launched for much of the functionality of the app to work.
