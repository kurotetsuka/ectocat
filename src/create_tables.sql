
create database if not exists ectocat;

use ectocat;

create table if not exists Requests (
	repository varchar( 128)
);

create table if not exists Meta (
	repository varchar( 128),
	update_time datetime
);

create table if not exists Commits (
	sha varchar( 40),
	author varchar( 128),
	repository varchar( 128),
	datetime datetime,
	message varchar( 4096)
);

create table if not exists CommitStats (
	repository varchar( 128),
	week datetime,
	contributor_count int
);

create table if not exists CommitEmotion (
	sha varchar( 40),
	repository varchar( 128),
	anger float,
	fear float,
	joy float,
	love float,
	sadness float,
	surprise float
);

create table if not exists Issues (
	repository varchar( 128),
	id bigint,
	open_time datetime,
	close_time datetime
);

create table if not exists IssueComments (
	issue_id bigint,
	comment_id bigint,
	author varchar( 128),
	repository varchar( 128),
	datetime datetime,
	message varchar( 4096)
);

create table if not exists IssueStats (
	repository varchar( 128),
	week datetime,
	open_count bigint,
	opened bigint,
	closed bigint,
	staleness bigint
);

create table if not exists IssueEmotion (
	issue_id bigint,
	comment_id bigint,
	repository varchar( 128),
	anger float,
	fear float,
	joy float,
	love float,
	sadness float,
	surprise float
);
