
use ectocat;

drop table if exists Requests, Meta,
	Commits, CommitStats, CommitEmotion,
	Issues, IssueComments, IssueStats, IssueEmotion;
