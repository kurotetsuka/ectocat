#!/usr/bin/env ruby

# library imports
require 'json'
require 'octokit'
require 'pstore'

# local imports
require_relative 'nlp'

module Ectocat

class Miner
	@@cache_dir = "data/cache/"
	@@mine_limit_enabled = false
	@@mine_limit = 50

	#constructor
	def initialize( token)
		# load emoticon scorer
		@emoticon_scorer = Nlp::SymbolScorer.new(
			"config/emoticons.json")
		# load emoji scorer
		@emoji_scorer = Nlp::SymbolScorer.new(
			"config/emoji.json")

		# load github api client
		@gh_client = Octokit::Client.new(
			access_token: token)

		# get db options
		printf( "connecting to database\n")
		db_host = ENV.key?( 'ECT_db_host') ?
			ENV['ECT_db_host'] : "localhost"
		db_user = ENV.key?( 'ECT_db_user') ?
			ENV['ECT_db_user'] : "ectocat"
		db_pass = ENV.key?( 'ECT_db_pass') ?
			ENV['ECT_db_pass'] : "ectocat"
		db_name = ENV.key?( 'ECT_db_name') ?
			ENV['ECT_db_name'] : "ectocat"
		# connect to database
		@db_client = Mysql2::Client.new(
			host: db_host,
			username: db_user,
			password: db_pass,
			database: db_name,
			connect_timeout: 3)
		printf( "connected to database\n")

		# print oauth data
		printf( "oauth user: %s\n", @gh_client.user.name)
		# dump rate limit data
		printf( "oauth req remaining: %d\n", @gh_client.rate_limit.remaining())
	end

	def load_repos( filename = "config/repos.txt")
		@repos = Array.new
		# add each repo from config file
		data = File.read( filename)
		for entry in data.split("\n")
			# ignore comments
			if ! ( entry[ 0] == '#') then
				@repos << entry
			end
		end
		printf( "repos to mine: %s\n", @repos)
	end

	def mine
		for repo in @repos do
			printf( "mining repo %s\n", repo)
			mine_repo( repo)
		end
		printf( "oauth req remaining: %d\n", @gh_client.rate_limit.remaining())
	end

	def mine_repo( repo_name)
		repo = @gh_client.repo( repo_name)

		# mine commits and issues
		printf( "digging through commit data\n")
		mine_commits( repo, repo_name)
		printf( "digging through issue data\n")
		mine_issues( repo, repo_name)

		# set update time
		printf( "setting last update time\n")
		check = @db_client.query(
			"select update_time from Meta" +
			" where repository='#{repo_name}'")
		update_time = Time.now.utc
		if check.size == 0 then
			query = "insert into Meta values" +
				" ('#{repo_name}', '#{update_time}');";
		else
			query = "update Meta" +
				" set update_time = '#{update_time}'" +
				" where repository='#{repo_name}'";
		end
		printf("query: %s\n", query)
		result = @db_client.query( query);
	end

	def mine_commits( repo, repo_name)
		printf( "mining commits for repo %s\n", repo_name)

		# get commits
		@gh_client.auto_paginate = false
		queue = []

		branches = repo.rels[:branches].get.data
		for branch in branches do
			queue << branch.commit.sha
		end

		count = 0
		while ! queue.empty? do
			sha = queue.pop
			commit = @gh_client.commit( repo_name, sha)

			# push parent commits to queue
			for parent in commit.parents do
				queue << parent.sha
			end

			# check if we've already got this commit
			check = @db_client.query(
				"select sha from Commits" +
				" where sha='#{sha}'" +
				" and repository='#{repo_name}'")
			if check.size > 0 then
				printf( "skipping commit %s\n", sha)
				next
			end
			printf( "mining commit %s\n", sha)

			# construct query
			author = commit.author.nil? ?
				commit.commit.author.name : commit.author.login
			author = @db_client.escape( author)
			commit_dt = commit.commit.author.date
			message = @db_client.escape(
				commit.commit.message)
			query = "insert into Commits values " +
				"('#{sha}', '#{author}', '#{repo_name}', " +
				"'#{commit_dt}', '#{message}');"

			# execute query
			result = @db_client.query( query)
			#printf("query: %s\n", query)
			#printf("query result: %s\n", result)

			# try not to exceed rate limit
			if @gh_client.rate_limit.remaining() < 10 then
				printf( "rate-limit remaining low - waiting for reset\n")
				sleep( @gh_client.rate_limit.resets_in)
				return
			end

			# stop if mining limit reached
			if @@mine_limit_enabled then
				printf( "mine_limit_enabled: %s\n", @@mine_limit_enabled)
				if ( count += 1) > @@mine_limit then
					break
				end
			end
		end
	end

	def mine_issues( repo, repo_name)
		printf( "mining issues for repo %s\n", repo_name)

		# check for issues
		if ! repo.has_issues()
			raise "[error] repository does not have issues enabled"
		end

		# get issues
		@gh_client.auto_paginate = true
		issues = @gh_client.issues( repo_name,
			sort: "created",
			direction: "asc")

		count = 0
		for issue in issues
			issue_id = issue.number
			printf( "mining issue %d\n", issue_id)

			# check if we've already got this issue
			check = @db_client.query(
				"select id, open_time, close_time from Issues" +
				" where id='#{issue_id}'" +
				" and repository='#{repo_name}'")
			if check.size > 0 then
				printf( "skipping issue %d\n", issue_id)
				next
			end

			# insert basic issue data
			opened_time = issue.created_at
			closed_time = issue.closed_at
			closed_time = closed_time.nil? ? "null" : "'#{closed_time}'"
			query = "insert into Issues value " +
				"('#{repo_name}',#{issue_id},'#{opened_time}',#{closed_time});";
			#printf("query: %s\n", query)
			result = @db_client.query( query)
			#printf("query result: %s\n", result)

			query = "insert into IssueComments values "
			# add issue body
			comment_id = 0
			comment_user = @db_client.escape( issue.user.login)
			comment_dt = issue.created_at
			comment_body = @db_client.escape( issue.body)
			query << "( #{issue_id}, #{comment_id}, '#{comment_user}', " +
				"'#{repo_name}', '#{comment_dt}', '#{comment_body}')"

			# add every comment
			comments = issue.rels[:comments].get().data
			for comment in comments
				comment_user = comment.user.login
				comment_id = comment.id
				comment_dt = comment.created_at
				comment_body = @db_client.escape( comment.body)
			query << ", ( #{issue_id}, #{comment_id}, '#{comment_user}', " +
				"'#{repo_name}', '#{comment_dt}', '#{comment_body}')"
			end

			# finish query
			query << ";"
			#printf("query: %s\n", query)
			result = @db_client.query( query)
			#printf("query result: %s\n", result)

			# try not to exceed rate limit
			if @gh_client.rate_limit.remaining() < 100 then
				printf( "rate-limit remaining low - waiting for reset\n")
				sleep( @gh_client.rate_limit.resets_in)
			end

			# stop if mining limit reached
			if @@mine_limit_enabled then
				if ( count += 1) > @@mine_limit then
					break
				end
			end
		end
	end

	def crunch_stats
		for repo in @repos do
			# crunch various stats
			printf( "crunching developer stats for repo %s\n", repo)
			crunch_commit_stats( repo)
			printf( "crunching issue stats for repo %s\n", repo)
			crunch_issue_stats( repo)
		end
	end

	def crunch_emotion
		for repo in @repos do
			# crunch emotion data
			printf( "mining emotion in commits\n")
			crunch_commit_emotion( repo)
			printf( "mining emotion in issue comments\n")
			crunch_issue_emotion( repo)
		end
	end

	def crunch_commit_stats( repo)
		#contributors = @gh_client.contributors_stats( repo)
	end

	def crunch_issue_stats( repo)
	end

	def crunch_commit_emotion( repo)
	end

	def crunch_issue_emotion( repo)
	end

	# delete this:
	def test_emotion
		#get repository data
		repo_name = "rails/rails"

		# score
		emoticon_score = mine_emoticons( repo_name)
		emoji_score = mine_emoji( repo_name)
		printf( "repo issues emoticons score: %s\n", emoticon_score)
		printf( "repo issues emoji score: %s\n", emoji_score)
	end

	def mine_emoticons( repo_name)
		mine_symbols( repo_name, @emoticon_scorer)
	end

	def mine_emoji( repo_name)
		mine_symbols( repo_name, @emoji_scorer)
	end

	def mine_symbols( repo_name, scorer)
		# todo: check cache
		# check repo exists
		if ! @gh_client.repository?( repo_name)
			raise "no such repository"
		end
		# get repo
		repo = @gh_client.repo( repo_name)
		# check for issues
		if ! repo.has_issues()
			raise "repository does not have issues enabled"
		end
		# get issues
		issues = @gh_client.list_issues( repo_name,
			sort: "updated",
			direction: "desc")
		issue_comments = aggregate_issues( issues)
		# score
		score = Nlp::blank_score
		for issue in issue_comments
			for comment in issue
				# score the symbols in each comment
				comment_score = scorer.score( comment)
				for key in score.keys
					if comment_score.has_key? key
						score[ key] += comment_score[ key]
					end
				end
			end
		end
		score
	end

	def aggregate_issues( issues)
		result = Array.new
		for issue in issues
			comment_count = issue.comments
			if comment_count == 0
				next
			end
			comments = issue.rels[:comments].get()
			result << [ issue.body] + aggregate_comments( comments)
			break
		end
		result
	end

	def aggregate_comments( comments)
		result = Array.new
		for comment in comments.data
			result << comment.body
		end
		result
	end

end #class
end #module
