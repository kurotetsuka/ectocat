#!/usr/bin/env ruby

# library imports
require 'yajl'

# local imports
#require_relative 'asdf'

# 
module Ectocat

module Nlp

	# utility methods
	def self.blank_score
		{
			"anger" => 0.0,
			"fear" => 0.0,
			"joy" => 0.0,
			"love" => 0.0,
			"sadness" => 0.0,
			"surprise" => 0.0}
	end

	# classes

	# class that can score parrot emotions
	class ParrotScorer
		def score( text)
		end
	end

	# emoticons and emoji
	class SymbolScorer < ParrotScorer
		def initialize( config_glob)
			# get config file
			config_files = Dir.glob( config_glob)
			if config_files.empty?()
				fail "no valid config file"
			end
			# read config
			config_file = File.read( config_files.first())
			@symbols = Yajl::Parser.parse( config_file)
		end

		def score( text)
			# vars
			result = Nlp::blank_score
			count = 0;
			# search for each symbol
			for symbol in @symbols.keys do
				if text.include?( symbol)
					#printf( "found symbol %s\n", symbol)
					for emotion in @symbols[ symbol] do
						if result.key?( emotion) then
							result[ emotion] += 1.0
						else
							printf( "bad emotion found for symbol %s\n", symbol)
						end
					end
					count += 1
				end
			end
			# kinda normalize result
			if count > 0
				for emotion in result.keys do
					result[ emotion] /= count
				end
			end
			# return
			result
		end
	end

end #module
end #module
