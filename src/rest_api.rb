#!/usr/bin/env ruby

# library imports
require 'dotenv'
require 'json'
require 'sinatra'

# local imports
require_relative 'miner'
require_relative 'token'

module Ectocat

class RestApi < Sinatra::Base
	# sinatra config
	configure do
		#load and apply env file
		env = Dotenv::Environment.new( "config/dev.env")
		env.load
		env.apply!
		#set db options
		#set :db_pass, ENV['ECTOCAT_whatever']
		#set sinatra options
		set :server, %w[thin mongrel webrick]
		set :bind, 'localhost'
		#set :bind, '0.0.0.0'
		set :port, 7860
	end

	# project emotion
	get '/p/:org/:repo/emoticons' do
		content_type :json
		response['Access-Control-Allow-Origin'] = '*'
		repo_name = "#{params[:org]}/#{params[:repo]}"
		miner = settings.miner

		result = miner.mine_emoticons( repo_name)
		result.to_json()
	end
	get '/p/:org/:repo/emoji' do
		content_type :json
		response['Access-Control-Allow-Origin'] = '*'
		repo_name = "#{params[:org]}/#{params[:repo]}"
		miner = settings.miner

		result = miner.mine_emoji( repo_name)
		result.to_json()
	end

	# project metrics
	get '/p/:org/:repo/commits' do
		content_type :json
		response['Access-Control-Allow-Origin'] = '*'
		repo_name = "#{params[:org]}/#{params[:repo]}"

		result = {
			asdf0: "asdf0",
			asdf1: "asdf1",
			asdf2: "asdf2",}
		result.to_json()
	end

	get '/p/:org/:repo/contributors' do
		content_type :json
		response['Access-Control-Allow-Origin'] = '*'
		repo_name = "#{params[:org]}/#{params[:repo]}"

		result = {
			asdf0: "asdf0",
			asdf1: "asdf1",
			asdf2: "asdf2",}
		result.to_json()
	end

	# organization ?
	# developer ?

end
end #module
