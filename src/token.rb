#!/usr/bin/env ruby

# library imports

# local imports

# load a github token from the given file glob
def load_token( token_glob)
	# get token file
	token_files = Dir.glob( token_glob)
	if token_files.empty?()
		fail "no valid oath tokens"
	end
	# read and return
	token_file = token_files.first()
	File.read( token_file)
end
