#!/usr/bin/env ruby

# library imports
require 'dotenv'
require 'erubis'
require 'mysql2'
require 'sinatra'
require "sinatra/reloader"

# local imports

module Ectocat

class WebApp < Sinatra::Base
	# sinatra config
	configure do
		#load and apply env file
		env = Dotenv::Environment.new( "config/dev.env")
		env.load
		env.apply!
		#set sinatra options
		set :server, %w[thin mongrel webrick]
		enable :sessions
		set :bind, 'localhost'
		#set :bind, '0.0.0.0'
		set :port, 8080
		set :views, settings.root + '/../views'
		set :public_dir, settings.root + '/../static'
	end
	configure :development do
		register Sinatra::Reloader
	end

	# main pages
	get '/' do
		@head = erb :head
		erb :index
	end

	get '/overview' do
		@head = erb :head
		@repo_name = "rails/rails"
		@last_update = "2014-10-27"
		erb :overview
	end

	get '/search/:query' do
		@head = erb :head
		@query = params[:query]
		printf( "query: %s\n", @query)
		erb :search
	end

	get '/search' do
		@head = erb :head
		erb :search
	end

	#error pages
	not_found do
		#@head = erb :head
		#erb :not_found
		'This is an amazing 404 page!'
	end
end
end #module
